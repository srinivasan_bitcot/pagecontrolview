# PageControlView

[![CI Status](https://img.shields.io/travis/Srinivasan Raman/PageControlView.svg?style=flat)](https://travis-ci.org/Srinivasan Raman/PageControlView)
[![Version](https://img.shields.io/cocoapods/v/PageControlView.svg?style=flat)](https://cocoapods.org/pods/PageControlView)
[![License](https://img.shields.io/cocoapods/l/PageControlView.svg?style=flat)](https://cocoapods.org/pods/PageControlView)
[![Platform](https://img.shields.io/cocoapods/p/PageControlView.svg?style=flat)](https://cocoapods.org/pods/PageControlView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PageControlView is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'PageControlView', :git => 'https://srinivasan_bitcot@bitbucket.org/srinivasan_bitcot/pagecontrolview.git', :branch => 'master'

```
## Usage
To your view add the PageControlView as the Class in the story board
```ruby
import UIKit
import PageControlView

class ViewController: UIViewController {

    @IBOutlet weak var pageControlView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let control = PageControlView()
        pageControlView.addSubview(control)
        control.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        control.defaultIndicatorColor = .red
        control.currentIndicatorColor = .black
        control.indicatorSize = (Int(view.frame.width) / 7)
        control.pageCount = 7
        control.currentPage = 3
        
    }


}
```


## Author

Srinivasan Raman, srinivasan@bitcot.com

## License

PageControlView is available under the MIT license. See the LICENSE file for more info.
