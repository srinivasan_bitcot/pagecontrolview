
//
//  PageControl.swift
//  EPL
//
//  Created by Srinivasan Raman on 04/04/19.
//  Copyright © 2019 Srini. All rights reserved.
//

import UIKit
import SnapKit

public class PageControlView: UIView {
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setViewComponents()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var layers = [UIView]()
    public var indicatorSize = 0
    public var defaultIndicatorColor:UIColor = .red
    public var currentIndicatorColor:UIColor = .black
    func setViewComponents(){
        backgroundColor = .clear
    }
    
    public var pageCount: Int = 0 {
        didSet {
            updateNumberOfPages(pageCount)
        }
    }
    
    public var currentPage: Int = 0 {
        didSet {
            currentPage(self.currentPage)
        }
    }
    
    func updateNumberOfPages(_ count: Int) {
        for _ in 0...count{
            let indicator = UIView()
            indicator.backgroundColor = defaultIndicatorColor
            indicator.layer.cornerRadius = 5
            indicator.layer.masksToBounds = true
            layers.append(indicator)
        }
        addSubview(layers[0])
        layers[0].snp.makeConstraints { (make) in
            make.top.equalTo(5)
            make.bottom.equalTo(-5)
            make.left.equalToSuperview().offset(15)
            make.width.equalTo(indicatorSize-10)
        }
    }
    
    func currentPage(_ currentPage : Int){
        
        if currentPage == 0{
            layers[0].backgroundColor = currentIndicatorColor
        }
        for i in 1 ... layers.count-2 {
            if i == currentPage-1 {
                layers[i].backgroundColor = currentIndicatorColor
            }
            addSubview(layers[i])
            layers[i].snp.makeConstraints { (make) in
                make.top.equalTo(5)
                make.bottom.equalTo(-5)
                make.left.equalTo(layers[i-1].snp.right).offset(8)
                make.width.equalTo(indicatorSize-10)
            }
            
        }
    }
}

